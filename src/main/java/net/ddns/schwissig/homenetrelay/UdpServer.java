package net.ddns.schwissig.homenetrelay;

import io.netty.bootstrap.Bootstrap;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.channel.ChannelPipeline;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.Properties;

public class UdpServer {

  private static final Logger log = Logger.getLogger(UdpServer.class.getName());

  private static final String CONFIG_FILE_NAME = "config.properties";

  private String homeNetIpAddress;

  private int homeNetPort;

  private int listeningPort;

  public static void main(String[] args) {
    try {
      new UdpServer().run();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private UdpServer() {
    try {
      Properties config = loadConfig(CONFIG_FILE_NAME);

      log.info(String.format("Configuration file [%s] loaded.", CONFIG_FILE_NAME));

      listeningPort = Integer.parseInt(config.getProperty("port"));
      homeNetIpAddress = config.getProperty("homeNetServerHostname");
      homeNetPort = Integer.parseInt(config.getProperty("homeNetServerPort"));
    } catch (IOException e) {
      log.error(String.format("Unable to read config file [%s]. Resorting to default values.", CONFIG_FILE_NAME));

      listeningPort = 48618;
      homeNetIpAddress = "127.0.0.1";
      homeNetPort = 8080;
    }
  }

  private void run() throws Exception {
    final NioEventLoopGroup group = new NioEventLoopGroup();
    try {
      final Bootstrap b = new Bootstrap();
      b.group(group).channel(NioDatagramChannel.class)
          .option(ChannelOption.SO_BROADCAST, true)
          .option(ChannelOption.SO_SNDBUF, 1048576)
          .option(ChannelOption.SO_RCVBUF, 1048576)
          .handler(new ChannelInitializer<NioDatagramChannel>() {
            @Override
            public void initChannel(final NioDatagramChannel ch) {

              ChannelPipeline p = ch.pipeline();
              p.addLast(new IncomingPacketHandler(homeNetIpAddress, homeNetPort));
            }
          });

      // Bind and start to accept incoming connections.
      InetAddress address = InetAddress.getByName("0.0.0.0");

      log.info(String.format("Listening: Host [%s:%d].", address.toString(), listeningPort));
      b.bind(address, listeningPort).sync().channel().closeFuture().await();

    } finally {
      System.out.print("In Server Finally");
    }
  }

  /**
   * Load the specified config file.
   *
   * @return Config file loaded into a {@link Properties} object.
   * @throws IOException Throw if there is an error reading the given properties file from the classpath.
   */
  private Properties loadConfig(String propertiesFileName) throws IOException {
    Properties config = new Properties();
    InputStream inputStream = UdpServer.class.getClassLoader().getResourceAsStream(propertiesFileName);
    if (inputStream != null) {
      config.load(inputStream);
      return config;
    } else {
      throw new FileNotFoundException(String.format("Property file [%s] not found in the classpath.", propertiesFileName));
    }
  }
}
