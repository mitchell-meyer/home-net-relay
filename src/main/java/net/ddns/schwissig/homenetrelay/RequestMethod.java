package net.ddns.schwissig.homenetrelay;

public enum RequestMethod {

  GET,
  PUT
}
