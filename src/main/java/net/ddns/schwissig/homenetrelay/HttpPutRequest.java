package net.ddns.schwissig.homenetrelay;

import java.net.URL;

public class HttpPutRequest extends HttpRequest {

  public HttpPutRequest(URL url, String payload) {
    super(url, payload);
  }

  @Override
  public RequestMethod getRequestMethod() {
    return RequestMethod.PUT;
  }
}
