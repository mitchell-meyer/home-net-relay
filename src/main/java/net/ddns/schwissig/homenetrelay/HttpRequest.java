package net.ddns.schwissig.homenetrelay;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static io.netty.handler.codec.http.HttpHeaders.Names.USER_AGENT;

public abstract class HttpRequest implements Runnable {

  private static Logger log = Logger.getLogger(HttpRequest.class.getName());

  private final URL url;

  private final String payload;

  /**
   * TODO
   *
   * @param url URL to open the connection to.
   * @param payload Payload for the HTTP request.
   */
  public HttpRequest(URL url, String payload) {
    this.url = url;
    this.payload = payload;
  }

  /**
   * Retrieve the request type for this HTTP request.
   *
   * @return Request method.
   */
  public abstract RequestMethod getRequestMethod();

  @Override
  public void run() {
    try {
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod(getRequestMethod().name());
      connection.setRequestProperty("Accept", "application/json");
      connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
      connection.setDoOutput(true);
      OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), StandardCharsets.UTF_8);
      writer.write(payload);
      writer.close();

      int responseCode = connection.getResponseCode();

      log.info(String.format("%s Request [%s]; response code [%d].", getRequestMethod().name(), url.toString(), responseCode));
    } catch (MalformedURLException e) {
      log.error(String.format("%s Request [%s] had malformed URL: ", getRequestMethod().name(), url.toString()), e);
    } catch (ConnectException e) {
      log.error(String.format("%s Request [%s] unable to reach server.", getRequestMethod().name(), url.toString()));
    } catch (IOException e) {
      log.error(String.format("%s Request [%s] had I/O error: ", getRequestMethod().name(), url.toString()), e);
    }
  }
}
