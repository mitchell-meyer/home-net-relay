package net.ddns.schwissig.homenetrelay;

import java.net.URL;

public class HttpGetRequest extends HttpRequest {

  public HttpGetRequest(URL url) {
    super(url, null);
  }

  @Override
  public RequestMethod getRequestMethod() {
    return RequestMethod.GET;
  }
}