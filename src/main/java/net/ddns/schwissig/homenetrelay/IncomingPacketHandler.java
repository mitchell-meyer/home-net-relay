package net.ddns.schwissig.homenetrelay;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.time.LocalDateTime;

public class IncomingPacketHandler extends  SimpleChannelInboundHandler<DatagramPacket> {

  private static Logger log = Logger.getLogger(IncomingPacketHandler.class.getName());

  private final String homeNetHostName;

  private final int homeNetPort;

  public IncomingPacketHandler(String homeNetHostName, int homeNetPort) {
    this.homeNetHostName = homeNetHostName;
    this.homeNetPort = homeNetPort;
  }

  @Override
  protected void channelRead0(ChannelHandlerContext channelHandlerContext, DatagramPacket packet) throws Exception {
    final ByteBuf buf = packet.content();
    final int rcvPktLength = buf.readableBytes();
    final byte[] rcvPktBuf = new byte[rcvPktLength];
    buf.readBytes(rcvPktBuf);

    String[] data = new String(rcvPktBuf, 0, rcvPktBuf.length).split(":");
    if (data.length > 0) {
      if (data[0].equals("tempReading")) {
        String ipAddress = packet.sender().getAddress().getHostAddress();
        String reading = data[1];

        log.debug(String.format("Temperature reading from [%s]. Reading [%sF].", ipAddress, reading));

        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setScheme("http");
        uriBuilder.setHost(homeNetHostName);
        uriBuilder.setPort(homeNetPort);
        uriBuilder.setPath("/sensor/temperature/saveReading");

        String payload = new JSONObject()
            .put("ipAddress", ipAddress)
            .put("reading", reading)
            .put("readingTime", LocalDateTime.now().toString())
            .toString();

        new Thread(new HttpPutRequest(uriBuilder.build().toURL(), payload)).run();
      }
    }
  }
}
